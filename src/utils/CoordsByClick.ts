
export default class CoordsByClick {
   
   static header(event: React.MouseEvent<HTMLElement>) {
      const element = event.currentTarget.getBoundingClientRect();
      const windowInnerWidth = document.documentElement.clientWidth

      let left 
      let top = element.bottom + 10
      if (windowInnerWidth - element.right  < 320) {
         left = element.left - 340 
      } else {
         left = element.left
      }
      if (windowInnerWidth < 768) {
         left = 5
         top = 50
      }

      return {top, left}
   }

   static element(event: React.MouseEvent<HTMLElement>, indent: number = 0) {
      const element = event.currentTarget.getBoundingClientRect();
      const windowInnerWidth = document.documentElement.clientWidth

      let left
      let right
      let top = element.bottom + indent

      if (windowInnerWidth - element.right  < 320) {
         right = windowInnerWidth - element.right
      } else {
         left = element.left
      }
      if (windowInnerWidth < 576) {
         left = 5
         top = element.top
      }

      return {top, left, right}
   }
}