import generateId from './generateId';
import ITaskList from '../models/IListsTask';

export default function defaultTaskList(boardId: string) {
   const newTaskList: ITaskList[] = [
      {
         id: generateId(),
         order: 1,
         title: "Нужно сделать",
         list: [],
         boardId
      },
      {
         id: generateId(),
         order: 2,
         title: "В процессе",
         list: [],
         boardId
      },
      {
         id: generateId(),
         order: 3,
         title: "Готово",
         list: [],
         boardId
      }
   ]

   return newTaskList
}
