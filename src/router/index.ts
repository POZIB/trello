import Boards from "../pages/boards"

export interface IRoute {
   path: string
   element: React.ComponentType 
}

export enum RouteNames {
   REDIRECT = '*',
   MAIN = '/',
   BOARDS = '/boards',
}

export const routes: IRoute[] = [
   { path: RouteNames.REDIRECT, element: Boards },
   { path: RouteNames.BOARDS, element: Boards },
]
