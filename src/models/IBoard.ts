import IListsTask from "./IListsTask"

export default interface IBoard {
   id: string
   name: string
   listsTask: IListsTask[]
}