import ITask from "./ITask"

export default interface IListsTask { 
   id: string
   order: number
   title: string,
   list: ITask[]
   boardId: string
}
