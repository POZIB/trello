export default interface ICoords {
   top?: string | number
   left?: string | number
   right?: string | number
   bottom?: string | number
}