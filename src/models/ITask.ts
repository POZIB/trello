export default interface ITask {
   id: string,
   order: number
   title: string,
   description?: string
   listId: string
}