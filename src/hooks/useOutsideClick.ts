import React from "react"

type returnType<T> = [
   React.LegacyRef<T>,
   boolean, 
   (value: React.SetStateAction<boolean>) => void
 ]
 

const useOutsideClick = <T extends HTMLElement>(state: boolean = false): returnType<T> => {
   const [visible, setVisible] = React.useState(state)
   const refContainer = React.useRef<T>(null);

   React.useEffect(() => {
      if (visible) {
         document.addEventListener("mousedown", handleOutsideClicks);
      }

      return () => {
         document.removeEventListener("mousedown", handleOutsideClicks);
      };
   }, [visible]);

   const handleOutsideClicks = (event: MouseEvent) => {
      if(visible && refContainer.current && !refContainer.current.contains(event.target as Node)){
         setVisible(false)
      };
   };

   return [refContainer, visible, setVisible]
}

export default useOutsideClick

