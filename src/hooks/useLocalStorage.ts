import React from "react";

type returnType<T> = [
  T | null,
  (value: T) => void
]

const useLocalStorage = <T>(key: string, defaultValue: string | object | number | null): returnType<T> => {
  const [value, setValue] = React.useState<T>(() => {
    let currentValue;

    try {
      currentValue = JSON.parse(
        localStorage.getItem(key) || String(defaultValue)
      ) || null
    } catch (error) {
      currentValue = defaultValue;
    }

    return currentValue;
  });

  React.useEffect(() => {
   if (value) {
      localStorage.setItem(key, JSON.stringify(value));
   }
  }, [value, key]);

  return [value, setValue];
};

export default useLocalStorage;