import React from 'react';

import { 
   BoardsContext, 
} from './index';

import IBoard from '../models/IBoard';
import useLocalStorage from '../hooks/useLocalStorage';
import IListsTask from '../models/IListsTask';
import ITask from '../models/ITask';
import { ICurrentMoveTask } from './types';


interface IContextContainer {
   children: React.ReactNode
}

const ContextContainer: React.FC<IContextContainer> = ({children}) => {
   const [, setValue] = useLocalStorage<IBoard[]>("trello", "");
    const [boards, setBoards] = React.useState<IBoard[]>([])

    React.useEffect(() => {
     if (boards.length) {
       setValue(boards)
     }
   },[boards])
   
   React.useEffect(() => {
      const hanlderUpdateData = () => {
         const data = JSON.parse(localStorage.getItem('trello') || "") as IBoard[]
         if (data) {
            setBoards(data)
         }
      }
   
      window.addEventListener("storage", hanlderUpdateData)
      return () => {
         window.removeEventListener("storage", hanlderUpdateData)
      }
   },[])


   const moveListTasks = async (currentBoard: IBoard, currentList: IListsTask, selectBoard: IBoard, selectListOrder: number) => {
      //находим индекс перемещаемого списка на текущей доске 
      const currListIndex = currentBoard.listsTask.findIndex(list => list.id === currentList.id)
      //удаляем список по его индексу из текущей доски  
      currentBoard.listsTask.splice(currListIndex, 1)

      //обновляем порядки списков на текущей доске
      const updatedCurrentBoard = currentBoard.listsTask.map((list, index) => 
         index >= currListIndex ? {...list, order: list.order - 1} : list
      )

      let _selectBoard: IListsTask[]
      const isMovingInCurrentBoard = currentList.boardId === selectBoard?.id

      if (isMovingInCurrentBoard) {
         _selectBoard = updatedCurrentBoard
      } else {
         _selectBoard = selectBoard.listsTask
      }

      const indexMoveList = selectListOrder - 1

      // перемещаем задачу в выбранный список по выбранному порядку
      const moveList = {...currentList, order: selectListOrder, boardId: selectBoard.id}
      _selectBoard.splice(indexMoveList, 0, moveList)

       // обновляем порядки cписков на выбранной доске
      const updatedSelectBoard = _selectBoard.map((list, index) =>
         index > indexMoveList ? {...list, order: list.order + 1} : list
      )

      
      const newBoards = boards.map(board => {
         if (board.id === selectBoard.id) {
            return {...board, listsTask: updatedSelectBoard}
         }
         if (board.id === currentBoard.id) {
            return {...board, listsTask: updatedCurrentBoard}
         }
         return board
      })

      setBoards(newBoards)
   }


   const moveTask = async (current: ICurrentMoveTask, selectBoard: IBoard, selectList: IListsTask, selectTaskOrder: number) => {
      const currentList = current.listTasks

      //находим индекс перемещаемой задачи в текущем списке 
      const currTaskIndex = currentList.list.indexOf(current.task)
      // удаляем задачу по ее индексу из текущего списка  
      currentList.list.splice(currTaskIndex, 1)

      // обновляем порядки задач в текущем списке
      const updatedCurrentList = currentList.list.map((task, index) => 
         index >= currTaskIndex ? {...task, order: task.order - 1} : task
      )

      let _selectList: ITask[]
      const isMovingInCurrentList = current.listTasks.id === selectList.id
      
      if (isMovingInCurrentList) {
         _selectList = updatedCurrentList
      } else {
         _selectList = selectList.list
      }
      
      const indexMoveTask = selectTaskOrder - 1
      // перемещаем задачу в выбранный список по выбранному порядку
      const moveTask = {...current.task, order: selectTaskOrder, listId: selectList.id}
      _selectList.splice(indexMoveTask, 0, moveTask)

      // обновляем порядки задач в выбранном списке
      const updatedSelectList = _selectList.map((task, index) =>
         index > indexMoveTask ? {...task, order: task.order + 1} : task
      )

      const listsTask = selectBoard.listsTask.map(lists => {
         if (lists.id === selectList.id) {
            return {...lists, list: updatedSelectList}
         }
         if (lists.id === current.listTasks?.id) {
            return {...lists, list: updatedCurrentList}
         }
         return lists
      })      

      const newBoard = {...selectBoard, listsTask: listsTask}  

      const newBoards = boards.map(board => 
         board.id === newBoard.id 
         ? newBoard
         : board
      )

      setBoards(newBoards)
   }

   const updateBoard = async (newBoard: IBoard) => {
      const newBoards = boards.map(board => 
         board.id === newBoard.id 
         ? newBoard
         : board
      )

      setBoards(newBoards)
   }

   const deleteListTasks = async (listTasks: IListsTask) => {
      const board = boards.find(board => board.id === listTasks.boardId)
      
      if (board) {
         board.listsTask = board?.listsTask.filter(list => list.id !== listTasks.id)
         updateBoard(board)
      }
   }

   const deleteTask = async (task: ITask, list: IListsTask) => {
      const board = boards.find(board => board.id === list.boardId)
      const listTask = board?.listsTask.find(listtasks => listtasks.id === list.id)
      
      if (board && listTask) {
         listTask.list = listTask.list.filter(t => t.id !== task.id)
         updateBoard(board)
      }
   }


   return (
      <>
         <BoardsContext.Provider value={{
            boards,
            setBoards,
            moveListTasks,
            moveTask,
            updateBoard,
            deleteListTasks,
            deleteTask
         }}>

            {children}  
         </BoardsContext.Provider>
      </>
   );
};

export default ContextContainer;