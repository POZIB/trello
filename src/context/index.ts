import { createContext } from "react";

import { 
   IAppContext, 
} from "./types";

export const BoardsContext = createContext<IAppContext>({} as IAppContext)

