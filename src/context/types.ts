
import IBoard from "../models/IBoard";
import IListsTask from "../models/IListsTask";
import ITask from "../models/ITask";

export interface ICurrentMoveTask {
   board: IBoard 
   listTasks: IListsTask
   task: ITask
}

export interface IAppContext {
   boards: IBoard[],
   setBoards: (value: IBoard[]) => void
   moveListTasks: (currentBoard: IBoard, currentList: IListsTask, selectBoard: IBoard, selectListOrder: number) => Promise<void>
   moveTask:(current: ICurrentMoveTask, selectBoard: IBoard, selectList: IListsTask, selectTaskOrder: number) => Promise<void>
   updateBoard: (newBoard: IBoard) => Promise<void>
   deleteListTasks: (list: IListsTask) => Promise<void>
   deleteTask: (task: ITask, list: IListsTask) => Promise<void>
}
