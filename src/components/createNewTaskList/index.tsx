import React from 'react';
import { useParams } from 'react-router-dom';

import { BoardsContext } from '../../context';

import useOutsideClick from '../../hooks/useOutsideClick';
import generateId from '../../utils/generateId';

import Button from '../UI/button';
import ButtonClose from '../UI/buttonClose';
import Input from '../UI/input';

import './style.scss';

const CreateNewTaskList = () => {
   let { boardId } = useParams();
   const {boards, setBoards} = React.useContext(BoardsContext)
   const [refContainerOpenInput, openInput, setOpenInput] = useOutsideClick<HTMLDivElement>()
   const [titleList, setTitleList] = React.useState("")

   const handlerToggler = () => {
      setOpenInput(prev => !prev)
      setTitleList("")
   }

   const handlerSubmit = (event: React.FormEvent<HTMLFormElement> ) => {
      event.preventDefault()

      const board = boards.find(item => item.id === boardId)
      const order = board?.listsTask.length ? board?.listsTask.length + 1 : 1

      if (board) {
         const newListTasks = {id: generateId(), order, title: titleList, list: [], boardId: board.id}
         board?.listsTask.push(newListTasks)
      }
      
      if (titleList && boardId && board) {
         const newBoards = boards.map(item => item.name === board.name ? board : item)
         setBoards(newBoards)
      }
      
      handlerToggler()
   }

   

   return (
      <div 
         className={`add-new-task-list ${openInput ? 'adding' : ''}`}
      >
         <form className='add-new-task-list__form' onSubmit={handlerSubmit}>
            {openInput
            ?  <div ref={refContainerOpenInput}> 
                  <Input 
                     autoFocus
                     value={titleList} 
                     onChange={(e) => setTitleList(e.target.value)}
                  />
                  <div className='add-new-task-list__form__container-buttons'>
                     <Button 
                        className='add-new-task-list__button-add'
                        type='submit'
                        variant='primary' 
                     >
                        Добавить список
                     </Button>
                     <ButtonClose type='button' onClick={handlerToggler} />
                  </div>
               </div>
            :  <button 
                  className='add-new-task-list__button' 
                  type="button"
                  onClick={handlerToggler}
               >
                  <div className="add-new-task-list__button__content">
                     <span className="add-new-task-list__button__icon icon-add left"/>
                     <span 
                        className="add-new-task-list__button__title"
                     >
                        Добавить еще один список
                     </span>
                  </div>
               </button>
            }
         </form>
      </div>
   );
};

export default CreateNewTaskList;