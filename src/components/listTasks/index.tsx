import React from 'react';
import { useParams } from 'react-router-dom';

import { BoardsContext } from '../../context';
import IBoard from '../../models/IBoard';
import Button from '../UI/button';
import ButtonClose from '../UI/buttonClose';
import Input from '../UI/input';
import TextArea from '../UI/textArea';
import TaskItem from '../taskItem';
import ButtonMenuHorizontal from '../UI/buttonMenuHorizontal';

import ListActions from '../../components/dropDawnMenu/listActions';

import generateId from '../../utils/generateId';
import CoordsByClick from '../../utils/CoordsByClick';

import ITask from '../../models/ITask';
import IListsTask from '../../models/IListsTask';
import ICoords from '../../models/ICoords';

import useOutsideClick from '../../hooks/useOutsideClick';

import './style.scss';

const ListTasks: React.FC<IListsTask> = (listTask) => {
   let { boardId } = useParams();
   const {boards, updateBoard} = React.useContext(BoardsContext)

   const [titleCurrentList, setTitleCurrentList] = React.useState(listTask.title)
   const [newTitleTask, setNewTitleTask] = React.useState("")
   const [coordsListActions, setCoordsListActions] = React.useState<ICoords>()
   const [refContainerNewTask, openInputNewTask, setOpenInputNewTask] = useOutsideClick<HTMLDivElement>()

   const selectDatas = React.useMemo(() => {
      const currBoardId = boardId || listTask.boardId
      const currentBoard = boards.find(board => board.id === currBoardId)
      const currentTaskList = currentBoard?.listsTask.find(item => item.id === listTask.id)

      return {currentBoard, currentTaskList}
   }, [boards])

   const togglerInputNewTask = () => {

      setOpenInputNewTask(prev => {
         if (prev) {
            setNewTitleTask("")
         }

         return !prev
      })
   }

   const onSubmitAddNewTask = () => {
      if (!newTitleTask || !listTask.boardId) return
      
      const {currentBoard, currentTaskList} = selectDatas
      
      if (currentBoard && currentTaskList) {
         const order = currentTaskList.list.length ? currentTaskList.list.length + 1 : 1
         const newTask = {id: generateId(), order, title: newTitleTask, listId: currentTaskList.id}

         currentTaskList.list.push(newTask)
         updateAllBoards(currentBoard)
      }

      setNewTitleTask("")
      togglerInputNewTask()
   }


   const updateTitleList = () => {
      if (!listTask.boardId) return

      const {currentBoard, currentTaskList} = selectDatas
      
      if (currentBoard && currentTaskList) {
         currentTaskList.title = titleCurrentList
         updateAllBoards(currentBoard)
      }
   }

   const updateAllBoards = (newBoard: IBoard) => {
      updateBoard(newBoard)
   }

   const handlerUpdateTitleListByBlur = () => {
      updateTitleList()
   }

   const handlerUpdateTitleListByKeyDown 
      = (event: React.KeyboardEvent<HTMLInputElement>) => {
      if (event.key === "Enter") {
         (event.target as HTMLInputElement).blur()
         updateTitleList()
      }
   }

   const handlerSelectItem = (task: ITask) => {
      const {currentBoard, currentTaskList} = selectDatas
   }

   const handlerOpenListActions = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
      const coords = CoordsByClick.element(e, 2)
      setCoordsListActions(coords)
   }

   const hanlderAddNewTask = () => {
      setCoordsListActions(undefined)
      setOpenInputNewTask(true)
   }

   return (
      <div className='task-list'>
         <div className="task-list__container">
            <div className="task-list__header">
               <Input 
                  className='task-list__input-title'
                  variant="Text" 
                  value={titleCurrentList} 
                  onChange={e => setTitleCurrentList(e.target.value)}
                  onBlur={handlerUpdateTitleListByBlur}
                  onKeyDown={handlerUpdateTitleListByKeyDown}
               />
               <ButtonMenuHorizontal onClick={handlerOpenListActions}/>
               {
                  coordsListActions && 
                  <ListActions 
                     coords={coordsListActions}
                     currentList={listTask}
                     onClose={() => setCoordsListActions(undefined)} 
                     onAddNewTask={hanlderAddNewTask}
                  />
               }
            </div>
            <div className="task-list__body">   
               <div className="task-list__list">
                  {
                     listTask.list.map((task) => 
                        <TaskItem 
                           key={task.id} 
                           task={task} 
                           onClick={() => handlerSelectItem(task)}
                        />
                     )
                  }
                  {
                     openInputNewTask && 
                     <div 
                        ref={refContainerNewTask}
                        className='task-list__textarea__wrapper' 
                     >
                        <div className="task-list__textarea__container">
                           <TextArea 
                              autoFocus
                              className='task-list__textarea'
                              placeholder="Ввести заголовок для этой карточки"
                              value={newTitleTask}
                              onChange={(e) => setNewTitleTask(e.target.value)}
                           />
                        </div> 
                        <div className='task-list__container-buttons'>
                           <Button 
                              variant='primary' 
                              onClick={onSubmitAddNewTask}
                              style={{marginRight: '8px'}}
                           >
                              Добавить карточку
                           </Button>
                           <ButtonClose onClick={togglerInputNewTask}/>
                        </div>
                     </div>
                  }
                  </div>  
            </div>
            <div 
               className={`task-list__bottom ${openInputNewTask ? 'hidden' : ''}`}
            >
               {
                  !openInputNewTask &&
                  <button 
                     className='button-add-new-task' 
                     onClick={togglerInputNewTask}
                  >
                     <div className="button-add-new-task__contaner">
                        <span className="button-add-new-task__icon icon-add"/>
                        <span className="button-add-new-task__title">Добавить карточку</span>
                     </div>
                  </button>
               }
            </div>
         </div>
      </div>
   );
};

export default ListTasks;