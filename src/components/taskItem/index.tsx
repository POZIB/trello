import React from 'react';
import { Link } from 'react-router-dom';

import ITask from '../../models/ITask';

import './style.scss';


interface IProps {
   task: ITask
   onClick?: () => void
}

const TaskItem: React.FC<IProps> = ({task, onClick}) => {

   const handlerClickEditItem = (event: React.MouseEvent<HTMLSpanElement>) => {
      event.preventDefault()
   }

   return (
      <Link className="task-item" to={`task/${task.id}`} onClick={onClick}>
         <div 
            className='task-item__edit' 
            onClick={(e) => handlerClickEditItem(e)}
         >
            <span className='icon-edit' />
         </div>
         <div className="task-item__content">
            <span>{task.title}</span>
         </div>
      </Link>
   );
};

export default TaskItem;