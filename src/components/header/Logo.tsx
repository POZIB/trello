import React from 'react';
import { Link } from 'react-router-dom';

const Logo = () => {
   return (
      <Link to="/" className="header__logo">
         <div className='logo__container'>
            <div className="logo"/>
         </div>
      </Link> 
   );
};

export default Logo;