import React from 'react';


import ICoords from '../../models/ICoords';
import CoordsByClick from '../../utils/CoordsByClick';
import CreateNewBoard from '../dropDawnMenu/createNewBoard';
import Button from '../UI/button';
import Logo from './Logo';

import './style.scss';


const Header = () => {
   const [coordsMenuCreate, setCoordsMenuCreate] = React.useState<ICoords>()

   const handleOpenCreateNewBoard = (event:  React.MouseEvent<HTMLButtonElement>) => {
      const coords = CoordsByClick.header(event)
      setCoordsMenuCreate(coords)
   }

   return (
      <header className='header'>
         <div className='header__container'>
            <div className="header__item">
               <Logo />
            </div>
            <div className="header__item">
               <Button variant='default-dark' onClick={handleOpenCreateNewBoard}>
                  Создать
               </Button>
            </div>
         </div>
         {coordsMenuCreate && 
            <CreateNewBoard 
               coords={coordsMenuCreate} 
               onClose={() => setCoordsMenuCreate(undefined)}
            />
         }
      </header>
   );
};

export default Header;