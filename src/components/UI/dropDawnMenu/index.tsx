import React from 'react';

import ButtonClose from '../buttonClose';
import {ReactComponent as IconArrowLeft}from '../../../assets/icons/arrow-left.svg';
import ICoords from '../../../models/ICoords';

import './style.scss';
import ReactDOM from 'react-dom';

interface IDropDawnMenu {
   title: string
   coords?: ICoords
   open: boolean,
   onClose: () => void,
   onGoBack?: () => void,
   children?: React.ReactNode
}

const DropDawnMenu: React.FC<IDropDawnMenu> = ({title, coords, open, onClose, onGoBack, children}) => {
   const ref = React.useRef<HTMLDivElement>(null);

   React.useEffect(() => {
      document.addEventListener("mousedown", handleOutsideClicks);

      return () => {
         document.removeEventListener("mousedown", handleOutsideClicks);
      };
   }, []);

   const handleOutsideClicks = (event: MouseEvent) => {
      if(open && ref.current && !ref.current.contains(event.target as Node)){
         handlerClose()
      };
   };

   const handlerClose = () => {
      onClose()
   }

   const handlerGoBack = (event:  React.MouseEvent<HTMLAnchorElement>) => {
      event.preventDefault()

      onGoBack && onGoBack()
   }

   return (
      ReactDOM.createPortal(
      <div ref={ref} style={coords} className='dropdawn-menu'>
         <div className='dropdawn-menu__header'>
               {onGoBack &&
                  <div className='dropdawn-menu__button-back'>
                     <a href="#" onClick={handlerGoBack}><IconArrowLeft /></a>
                  </div>
               }
               <span className='dropdawn-menu__title'>{title}</span>
               <div className='dropdawn-menu__button-close'>
                  <ButtonClose onClick={handlerClose}/>
               </div>
            </div>
         <div className='dropdawn-menu__body'>
            {children}
         </div>
      </div>, 
      document.getElementById('root') as HTMLElement)
   );
};

export default DropDawnMenu;