import React from 'react';

import {ReactComponent as Icon}from '../../../assets/icons/close.svg';

import './style.scss';

const ButtonClose:React.FC<React.ButtonHTMLAttributes<HTMLButtonElement>> = (props) => {
   return (
      <button {...props} className={`button_close ${props.className || ''}`} type='button'>
         <span className='button_close__body'>
            <Icon/>
         </span>
      </button>
   );
};

export default ButtonClose;