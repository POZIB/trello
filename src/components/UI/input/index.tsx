import React from 'react';

import './style.scss';

interface IInputProps extends React.InputHTMLAttributes<HTMLInputElement> {
   variant?: 'Standard' | 'Text'
   title?: string
   required?: boolean
   requiredMessage?: string
   error?: boolean
}

const Input:React.FC<IInputProps> = ({
   title, 
   required, 
   error, 
   requiredMessage,
   variant = 'Standard',  
   ...props
}) => {

   return (
      <div className='input-wrapper'>
         <div className='input__title'>
            {title}
            {required && <span className='required'>*</span>}
         </div>
         <input 
            {...props}
            className={`input ${variant} ${!props.disabled && error ? 'error' : ''} ${props.className || ''}`} 
            type="text" 
         />
         {
            !props.disabled && error && 
            <span className='input_error-message'>
               {requiredMessage || "Обязательное поле"}
            </span>
         }
      </div>
   );
};

export default Input;