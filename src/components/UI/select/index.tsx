import React from 'react';

import './style.scss';

interface ISelect extends React.InputHTMLAttributes<HTMLSelectElement>{
   children?: React.ReactElement<HTMLOptionElement> | Array<React.ReactElement<HTMLOptionElement>>
   label: string,
   valueDisplay?: string | number
   labelGroup?: string
}

const Select: React.FC<ISelect> = ({label, valueDisplay, labelGroup, children, ...props}) => {

   const childrenWithProps = React.Children.map(children, (child) => {
      if (!React.isValidElement(child)) return child
      
      if (child.props.value === props.value) {
         return  React.cloneElement(child, {...child.props})
      } else {
         return child
      }
    })

   return (
      <div className='select__wrapper'>
         <span className='select__label'>{label}</span>
         <span className='select__current-value'>{valueDisplay || 'Н/Д'}</span>
         <select {...props} className={`select ${props.className || ''}`}>
            {labelGroup 
               ?  
               <optgroup label={labelGroup}>
                  {childrenWithProps}
               </optgroup>
               : 
               childrenWithProps
            }
         </select>
      </div>
   );
};

export default Select;