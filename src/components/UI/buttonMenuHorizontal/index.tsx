import React from 'react';

import {ReactComponent as Icon}from '../../../assets/icons/menu-horizontal.svg';

import './style.scss';

const ButtonMenuHorizontal: React.FC<React.ButtonHTMLAttributes<HTMLButtonElement>> = (props) => {
   return (
      <button {...props} className={`button_menu-horizontal ${props.className || ''}`} type='button'>
      <span className='button_menu-horizontal__body'>
         <Icon/>
      </span>
   </button>
   );
};

export default ButtonMenuHorizontal;