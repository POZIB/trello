import React from 'react';

import './style.scss';

interface IButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement>  {
   variant?: 'primary' | 'default' | 'default-dark'
}

const Button:React.FC<IButtonProps> = ({variant = 'default', ...props}) => {
   return (
      <button 
         {...props} 
         className={`button ${variant} ${props.className || ''}`} >
      </button>
   );
};

export default Button;