import React from 'react';

const TextArea: React.FC<React.InputHTMLAttributes<HTMLTextAreaElement>> = (props) => {

   const handlerChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
      if (!props.onChange) return

      e.target.style.height = "30px";
      e.target.style.height = `${e.target.scrollHeight}px`;

      props.onChange(e)
   };

   return (
      <textarea 
         {...props}
         onChange={handlerChange}
      />
   );
};

export default TextArea;