import React from 'react';
import { Link } from 'react-router-dom';

import IBoard from '../../models/IBoard';

import './style.scss';

interface IProps {
   data: IBoard
}

const Board:React.FC<IProps> = ({data}) => {
   return (
      <Link className="board" to={'/board/' + data.id}>
         <div className='board__content'>
            <div className='board__title'>
               <span>{data.name}</span>
            </div>
            <div className='board__sub-title'>
               <span></span>
            </div>
         </div>
      </Link>
   );
};

export default Board;