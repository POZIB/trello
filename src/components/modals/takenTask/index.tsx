import React from 'react';
import ReactDOM from 'react-dom';
import { useNavigate, useParams } from 'react-router-dom';

import { BoardsContext } from '../../../context';
import ICoords from '../../../models/ICoords';

import CoordsByClick from '../../../utils/CoordsByClick';
import MoveTask from '../../dropDawnMenu/moveTask';

import Button from '../../UI/button';
import ButtonClose from '../../UI/buttonClose';
import Input from '../../UI/input';

import './style.scss';

const TakenTaskModal = () => {
   const navigate = useNavigate()
   const {boardId, taskId} = useParams()
   const {boards, updateBoard, deleteTask} = React.useContext(BoardsContext)

   const [titleTask, setTitleTask] = React.useState("")
   const [currentList, setCurrentList] = React.useState("")
   const [loading, setLoading] = React.useState(true)
   const [coordsMoveTask, setCoordsMoveTask] = React.useState<ICoords>()

   React.useEffect(() => {
     const { listTasks, task } = currentValues

      if (listTasks?.title && task?.title) {
         setCurrentList(listTasks.title)
         setTitleTask(task.title)
         setLoading(false)
      }
   },[boards])

   const currentValues = React.useMemo(() => {
      const board = boards.find(item => item.id === boardId)
      const listTasks = board?.listsTask.find(item => 
         item.list.find(item => item.id === taskId)
      )
      const task = listTasks?.list.find(item => item.id === taskId)

      return {board, listTasks, task}
   }, [boards])


   const handleClose = (event: React.MouseEvent) => {
      navigate('/board/' + boardId)
   }

   const handlerUpdateTitleByKeyDown 
      = (event: React.KeyboardEvent<HTMLInputElement>) => {
      if (event.key === "Enter") {
         (event.target as HTMLInputElement).blur()

         updateTask()
      }
   }

   const updateTask = () => {
      const {board, task} = currentValues
      if (task && board && task.title !== titleTask) {   

         task.title = titleTask
         updateBoard(board)
      }
   }

   const handlerClickMove = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
      event.preventDefault()

      const coords = CoordsByClick.element(event, 5)
      setCoordsMoveTask(coords)
   }

   const handlerDeleteTask = () => {
      const { task, listTasks } = currentValues

      if (task && listTasks) {
         deleteTask(task, listTasks)
         navigate(-1)
      }
   }

   return (
      ReactDOM.createPortal(
         <>
            <div className='wrapper-overlay' style={{display: 'flex'}} onClick={handleClose}>
               <div className="taken-task" onClick={e => e.stopPropagation()}>
                  {loading 
                     ? <div className="taken-task__loader">Загрузка...</div>
                     : <>
                        <div className="taken-task__header" >
                           <Input 
                              className='taken-task__input'   
                              variant='Text' 
                              value={titleTask} 
                              onChange={e => setTitleTask(e.target.value)}
                              onBlur={updateTask}
                              onKeyDown={handlerUpdateTitleByKeyDown}
                           />
                           <div className='taken-task__current-task-list'>
                              В колонке{' '}
                              <a href="" onClick={handlerClickMove}>{currentList}</a>           
                           </div>
                           <div className='taken-task__button-close'  onClick={handleClose}>
                              <ButtonClose />
                           </div>
                        </div>
                        <div className="taken-task__body">
                           <div className="taken-task__main">
                              <div className='taken-task__container-description'>
                              <div className='taken-task__title-description'>
                                 <h3>Описание</h3>
                                 <Button >Изменить</Button>
                              </div>   
                              </div>
                           </div>
                           <div className="taken-task__sidebar">  
                              <div className="taken-task__sidebar__title">
                                 <h3>Действия</h3>
                              </div>
                              <div className="taken-task__sidebar__container">
                                 <div className='container_item'>
                                    <Button onClick={handlerClickMove}>Перемещение</Button>
                                 </div>
                                 <div className='container_item'>
                                    <Button onClick={handlerDeleteTask}>Удалить</Button>
                                 </div>
                              </div>   
                           </div>
                        </div>
                     </>
                  }
               </div>
            </div>
            {
                  coordsMoveTask && currentValues.board && currentValues.listTasks && currentValues.task &&
                  <MoveTask 
                     coords={coordsMoveTask} 
                     onClose={() => setCoordsMoveTask(undefined)}
                     board={currentValues.board}
                     listTasks={currentValues.listTasks}
                     task={currentValues.task}
                  />
            }
         </>, 
         document.getElementById('root') as HTMLElement)  
   );
};

export default TakenTaskModal;
