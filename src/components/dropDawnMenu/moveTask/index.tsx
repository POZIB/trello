import React from 'react';
import { useNavigate } from 'react-router-dom';

import { BoardsContext } from '../../../context';

import IBoard from '../../../models/IBoard';
import IListsTask from '../../../models/IListsTask';
import ITask from '../../../models/ITask';
import ICoords from '../../../models/ICoords';

import Button from '../../UI/button';
import Select from '../../UI/select';
import DropDawnMenu from '../../UI/dropDawnMenu';


import './style.scss';

interface IMoveTask {
   board: IBoard 
   listTasks: IListsTask
   task: ITask
   coords: ICoords
   onClose: () => void
}

const MoveTask: React.FC<IMoveTask> = ({coords, onClose, ...current}) => {
   const navigate = useNavigate()
   const {boards, moveTask} = React.useContext(BoardsContext)

   const [selectBoard, setSelectBoard] = React.useState<IBoard>(current.board)
   const [selectList, setSelectList] = React.useState<IListsTask>(current.listTasks)
   const [selectTaskOrder, setSelectTaskOrder] = React.useState<number | undefined>(current.task.order)

   const isNotSelectValues = !selectList || !selectBoard
   
   const handlerSelectBoard = (event: React.ChangeEvent<HTMLSelectElement>) => {
      const id = event.target.value
      const board = boards.find(board => board.id === id)

      if (!board) return

      setSelectBoard(board)

      const firstList = board?.listsTask[0]
      setSelectList(firstList)

      const lengthFirstList = firstList?.list.length
      const isMovingInCurrentBoard = current?.board?.id === board?.id

      if (lengthFirstList) {
            setSelectTaskOrder(isMovingInCurrentBoard ? lengthFirstList : lengthFirstList + 1)
      } else {
         setSelectTaskOrder(firstList ? 1 : undefined)
      }
   }

   const handlerSelectList = (event: React.ChangeEvent<HTMLSelectElement>) => {
      const id = event.target.value
      const listTask = selectBoard?.listsTask.find(list => list.id === id) 

      if (listTask) {
         setSelectList(listTask)
      }

      if (listTask?.list.length) {
         setSelectTaskOrder(listTask?.list.length + 1)
      } else {
         setSelectTaskOrder(1)
      }
   }

   const handlerSelectTaskOrder = (event: React.ChangeEvent<HTMLSelectElement>) => { 
      const order = +event.target.value
      setSelectTaskOrder(order)
   }

   const handlerSubmitMove =  () => {
      if (isNotSelectValues) return 

      if (current.board === selectBoard && 
         current.listTasks === selectList && 
         current.task.order === selectTaskOrder)
      {
         onClose()
         return
      }

      if (selectTaskOrder) {
         moveTask(current, selectBoard, selectList, selectTaskOrder)
         current.board.id !== selectBoard.id && navigate(-1)
      }

      onClose()
   }


   return (
      <DropDawnMenu
         title='Перемещение задачи'
         open={true} 
         coords={coords}
         onClose={onClose}
      >
         <div 
            className='move-tasks-list' 
         >
            <div className='move-tasks-list__container'>
               <div className='container__col'>
                  <Select 
                     label='Доска' 
                     valueDisplay={selectBoard?.name}
                     value={selectBoard?.id || 'Нет списков'} 
                     onChange={handlerSelectBoard}
                  >
                     {
                        boards.map(board => 
                           <option 
                              key={board.id} 
                              value={board.id}
                           >
                              {board.name}
                           </option>
                        )
                     }  
                  </Select>
               </div>

               <div className='container__row'>
                  <div style={{flex: 1}}>
                     <Select 
                        label='Список' 
                        value={selectList?.id}
                        valueDisplay={selectList?.title || 'Нет списков'}
                        onChange={handlerSelectList}
                     >
                     {
                        selectBoard.listsTask
                        .map(list => 
                           <option 
                              key={list.id} 
                              value={list.id}
                           >
                              {list.title}
                           </option>
                        )
                     } 
                     </Select>
                  </div>
                  <div>
                     <Select 
                        label='Позиция' 
                        valueDisplay={selectTaskOrder}
                        value={selectTaskOrder}
                        onChange={handlerSelectTaskOrder}
                        >
                     {
                        selectList?.list.length 
                        ?  <>
                              {selectList.list
                              .map(task => 
                                 <option 
                                    key={task.id} 
                                    value={task.order}
                                 >
                                    {task.order}
                                 </option>
                              )}
                              {
                                 current?.listTasks?.id !== selectList.id &&
                                 <option value={selectList.list.length + 1}>
                                    {selectList?.list.length + 1}
                                 </option>        
                              }
                           </>
                        : <option value={1}>1</option>
                     } 
                     </Select>       
                  </div>
               </div>
               <div className='container__row'>
                  <Button variant='primary' onClick={handlerSubmitMove}>Переместить</Button>
               </div>
            </div>
         </div>
      </DropDawnMenu>
   );
};

export default MoveTask;