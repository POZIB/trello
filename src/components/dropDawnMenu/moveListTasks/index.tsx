import React from 'react';

import { BoardsContext } from '../../../context';

import IBoard from '../../../models/IBoard';
import IListsTask from '../../../models/IListsTask';

import Button from '../../UI/button';
import Select from '../../UI/select';


import './style.scss';

interface IProps {
   currentList: IListsTask
   onClose: () => void
}

const MoveListTask: React.FC<IProps> = ({onClose, currentList}) => {
   const {boards, moveListTasks} = React.useContext(BoardsContext)

   const [selectBoard, setSelectBoard] = React.useState<IBoard>()
   const [selectListOrder, setSelectListOrder] = React.useState<number>(currentList.order || 1)

   React.useEffect(() => {
      const curBoard = boards.find(board => board.id === currentList.boardId)
        
      setSelectBoard(curBoard)
   }, [currentList])


   const handlerSelectBoard = (event: React.ChangeEvent<HTMLSelectElement>) => {
      const id = event.target.value
      const board = boards.find(board => board.id === id)
      setSelectBoard(board)

      const countListCurrBoard = board?.listsTask.length
      const isMovingInCurrentBoard = selectBoard?.id === board?.id

      if (countListCurrBoard) {
         setSelectListOrder(isMovingInCurrentBoard ? countListCurrBoard : countListCurrBoard + 1)
      } else {
         setSelectListOrder(1)
      }
   }

   const handlerSelectListOrder = (event: React.ChangeEvent<HTMLSelectElement>) => {
      const order = +event.target.value
      setSelectListOrder(order)
   }

   const handlerSubmitMove = () => { 
      const currentBoard = boards.find(board => board.id === currentList.boardId)

      if (currentBoard?.id === selectBoard?.id && currentList.order === selectListOrder){
         onClose()
         return
      }

      if (!currentBoard || !selectBoard) return

      moveListTasks(currentBoard, currentList, selectBoard, selectListOrder)
      
      onClose()
   }

   return (
      <div className='move-list'>
         <div className='move-list__container'> 

            <div className='container__row'>
               <Select 
                     label='Доска' 
                     valueDisplay={selectBoard?.name}
                     value={selectBoard?.id || 'Нет списков'} 
                     onChange={handlerSelectBoard}
               >
                  {
                     boards.map(board => 
                        <option 
                           key={board.id} 
                           value={board.id}
                        >
                           {board.name}
                        </option>
                     )
                  }  
               </Select>
            </div>

            <div className='container__row'>
               <Select 
                  label='Позиция' 
                  valueDisplay={selectListOrder}
                  value={selectListOrder|| 'Нет списков'} 
                  onChange={handlerSelectListOrder}
               >
                  <>
                     {
                        selectBoard?.listsTask.map(list => 
                           <option 
                              key={list.id} 
                              value={list.order}
                           >
                              {list.order}
                           </option>
                        )
                     } 
                     {
                        currentList.boardId !== selectBoard?.id &&
                        <option value={(selectBoard?.listsTask.length || 0) + 1}>
                           {(selectBoard?.listsTask.length || 0) + 1}
                        </option>        
                     } 
                  </>
               </Select>
            </div>

            <div className='container__row'>
               <Button variant='primary' onClick={handlerSubmitMove}>Переместить</Button>
            </div>

         </div>
      </div>
   );
};

export default MoveListTask;