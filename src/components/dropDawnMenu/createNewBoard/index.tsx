import React from 'react';
import { useNavigate } from 'react-router-dom';

import { BoardsContext } from '../../../context';
import defaultTaskList from '../../../utils/defaultTaskList';
import generateId from '../../../utils/generateId';

import Button from '../../UI/button';
import Input from '../../UI/input';
import DropDawnMenu from '../../UI/dropDawnMenu';

import ICoords from '../../../models/ICoords';

import './style.scss';

interface IProps {
   coords?: ICoords
   onClose: () => void
}

const CreateNewBoard: React.FC<IProps> = ({coords, onClose}) => {
   const navigate = useNavigate()
   const {boards, setBoards} = React.useContext(BoardsContext)
   const [nameNewBoard, setNameNewBoard] = React.useState('')

   const handleChange = (event:  React.ChangeEvent<HTMLInputElement>) => {
      setNameNewBoard(event.target.value)
   }

   const onSubmit = (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault()

      if (nameNewBoard) {
         const id = generateId()
         const newBoard = {id , name: nameNewBoard, listsTask: defaultTaskList(id) }
         
         setBoards([...boards, newBoard])
         onClose()

         navigate("/board/" + id)
      }
   }
   
   return (
      <DropDawnMenu 
         title='Создать доску'
         open={true}
         coords={coords}         
         onClose={onClose}
      >
         <form 
            className={'create-board'}
            onSubmit={onSubmit}
         >
            <div className='create-board__body'>
               <div className='create-board__row'>
                  <Input 
                     autoFocus
                     required 
                     title='Заголовок доски' 
                     requiredMessage={"👋 Укажите название доски."} 
                     value={nameNewBoard}
                     onChange={handleChange}
                     error={!!!nameNewBoard}/>
               </div>
               <div className='create-board__row'>
                  <Button variant='primary'>Создать</Button>
               </div>
            </div>
         </form>
      </DropDawnMenu>
   );
};

export default CreateNewBoard;