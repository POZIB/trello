import React from 'react';

import Button from '../../../UI/button';

import './style.scss';

interface IProps {
   onConfirm: () => void
   onUnConfirm: () => void
}

const DeleteListTasks: React.FC<IProps> = ({onConfirm, onUnConfirm}) => {
   
   return (
      <div className='delete-list'>
         <span className='delete-list__title'>Подтверждение удаления списка</span>
         <div className='delete-list__container-btn'>
            <Button variant='primary' onClick={onConfirm}>Удалить</Button>
            <Button variant='default' onClick={onUnConfirm}>Отменить</Button>
         </div>
      </div>
   );
};

export default DeleteListTasks;