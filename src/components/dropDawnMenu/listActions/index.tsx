import React from 'react';

import { BoardsContext } from '../../../context';
import MoveListTask from '../moveListTasks';
import DeleteListTasks from './DeleteListTasks';
import DropDawnMenu from '../../UI/dropDawnMenu';

import IListsTask from '../../../models/IListsTask';
import ICoords from '../../../models/ICoords';

import './style.scss';

interface IListActions {
   coords: ICoords
   currentList: IListsTask
   onClose: () => void
   onAddNewTask?: () => void
}

const ListActions: React.FC<IListActions> = ({coords, currentList, onClose, onAddNewTask}) => {
   const {deleteListTasks} = React.useContext(BoardsContext)
   const [item, setItem] = React.useState('Действие со списком')


   const handlerClickItem = (item: string, e?: React.MouseEvent<HTMLAnchorElement>) => {
      e?.preventDefault()
      setItem(item)
   }

   const handlerAddNewTask = (e: React.MouseEvent<HTMLAnchorElement>) => {
      e.preventDefault()
      onAddNewTask && onAddNewTask()
   }

   const handlerDeleteList = () => {
      deleteListTasks(currentList)
      onClose()
   }
  

   return (
      <DropDawnMenu
         title={item} 
         open={true} 
         onClose={onClose}
         coords={coords}
         onGoBack={
            item !== 'Действие со списком' 
            ? () => handlerClickItem('Действие со списком') 
            : undefined
         }
      >
         <div className='list-actions'>
            {
               item === 'Действие со списком' && 
               <ul className='list-actions__list'>
                  <li className='list-actions__item'>
                     <a 
                        href="/#" 
                        onClick={(e) => handlerClickItem('Перемещение списка', e)}
                     >
                        Переместить список...
                     </a>
                  </li>
                  <li className='list-actions__item'>
                     <a 
                        href="/#" 
                        onClick={handlerAddNewTask}
                     >
                        Добавить задачу...
                     </a>
                     </li>
                  <li className='list-actions__item'>
                     <a 
                        href="/#" 
                         onClick={(e) => handlerClickItem('Удаление списка', e)}
                     >
                        Удалить список     
                     </a>
                  </li>
               </ul>
            }
            {
               item === 'Перемещение списка' && 
               <MoveListTask currentList={currentList} onClose={onClose}/>
            }
            {
               item === 'Удаление списка' &&
               <DeleteListTasks onUnConfirm={onClose} onConfirm={handlerDeleteList}/>
            }
         </div>
      </DropDawnMenu>
   )
};

export default ListActions;