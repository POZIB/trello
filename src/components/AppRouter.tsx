import React from 'react';
import { Route, Routes } from 'react-router-dom';

import {routes} from '../router/index';

import TakenTaskModal from './modals/takenTask';
import TakenBoard from '../pages/takenBoard';

const AppRouter = () => {
   return (
      <>
         <Routes >
            {
               routes.map(route => 
                  <Route 
                     key={route.path}
                     path={route.path} 
                     element={<route.element />} 
                  />
               )
            }
            <Route path="/board/:boardId" element={<TakenBoard />}>   
               <Route path='task/:taskId'  element={<TakenTaskModal />}/>
            </Route>
         </Routes>
      </>
   );
};

export default AppRouter;