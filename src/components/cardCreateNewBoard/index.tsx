import React from 'react';

import ICoords from '../../models/ICoords';
import CoordsByClick from '../../utils/CoordsByClick';
import CreateNewBoard from '../dropDawnMenu/createNewBoard';

import './style.scss';

const CardCreateNewBoard: React.FC = () => {
   const [coordsCreateForm, setCoordsCreateForm] = React.useState<ICoords>()

   const handlerClick = (event:  React.MouseEvent<HTMLDivElement>) => {
      const coords = CoordsByClick.element(event, 5)
      setCoordsCreateForm(coords)
   }
   

   return (
      <>
         <div className="board-new-create" onClick={handlerClick}>
            <div className='board-new-create__body'>
               <div className='board-new-create__title'>
                  <span>Создать доску</span>
               </div>
               <div className='board-new-create__sub-title'>
               {false && <span>Осталось: 6</span>}
               </div>
            </div>
         </div>
         {coordsCreateForm && 
            <CreateNewBoard 
               coords={coordsCreateForm} 
               onClose={() => setCoordsCreateForm(undefined)}
            />
         }
      </>
   )
};

export default CardCreateNewBoard;