import React from 'react';

import AppRouter from './components/AppRouter';
import Layout from './pages/layout';
import ContextContainer from './context/ContextContainer';

function App() {

  return (
    <ContextContainer>
      <Layout>
        <AppRouter />
      </Layout>
    </ContextContainer>
  );
}

export default App;


