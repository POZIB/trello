import React from 'react';

import Board from '../../components/board';
import CardCreateNewBoard from '../../components/cardCreateNewBoard';
import useLocalStorage from '../../hooks/useLocalStorage';
import { BoardsContext } from '../../context';
import IBoard from '../../models/IBoard';

import './style.scss';

const Boards = () => {
   const [value] = useLocalStorage<IBoard[]>("trello", "");
   const {boards, setBoards} = React.useContext(BoardsContext)

   React.useEffect(() => {
      if (value) {
         setBoards(value)
      }
   },[])


   return (
      <div className='page-boards'>
         <div className="page-boards__body">
            <h2 className='page-boards__title'>ВАШИ РАБОЧИЕ ПРОСТРАНСТВА</h2>
            <ul className='page-boards__list'>
               {
                  boards.map((item) => 
                     <li className='page-boards__list__item' key={item.id}>
                        <Board data={item}/>
                     </li>
                  )
               }
               <li className='page-boards__list__item'>
                  <CardCreateNewBoard />
               </li>
            </ul>
         </div>
      </div>
   );
};

export default Boards;