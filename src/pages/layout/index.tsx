import React from 'react';
import Header from '../../components/header';

interface IProps {
   children?: React.ReactNode
}

const Layout:React.FC<IProps> = ({children}) => {
   return (
      <>
         <Header />
         <main className='main'>
            {children}
         </main>
      </>
   );
};

export default Layout;