import React from 'react';
import { Outlet, useParams } from 'react-router-dom';

import CreateNewTaskList from '../../components/createNewTaskList';
import ListTasks from '../../components/listTasks';
import { BoardsContext } from '../../context';
import useLocalStorage from '../../hooks/useLocalStorage';
import IBoard from '../../models/IBoard';
import IListsTask from '../../models/IListsTask';


import './style.scss';

const TakenBoard = () => {
   let { boardId } = useParams();
   const [value, _] = useLocalStorage<IBoard[]>("trello", "");
   const {boards, setBoards} = React.useContext(BoardsContext)
   const [listsTask, setListsTask] = React.useState<IListsTask[]>([])

   React.useEffect(() => {
      let board

      if (boardId) { 
         if (!boards.length && value?.length) {
            board = value.find(item => item.id === boardId)
            setBoards(value)
         } else {
            board = boards.find(item => item.id === boardId)
         }
         if (board) {  
            setListsTask(board.listsTask)
         }
       
      }
   },[boardId, boards])


   return (
      <div className='page-wrapper'>
         <div className='page-scroll'>
            {
               listsTask.map(item => 
                  <ListTasks key={item.id} {...item}/>
               )
            }
            <CreateNewTaskList />
         </div>
         <Outlet />
      </div>
   );
};

export default TakenBoard;